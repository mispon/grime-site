'use strict'

const isEmpty = (str) => {
    return !str || str.length === 0
}

const re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
const isEmailValid = (email) => {
    return re.test(email)
}

const isOnlyLatinChars = (str) => {
    const re = /^[a-zA-Z]*$/;
    return re.test(str);
}

module.exports = {isEmpty, isEmailValid, isOnlyLatinChars}