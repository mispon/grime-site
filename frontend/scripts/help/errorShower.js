'use strict'

const showError = (message) => {
    let error = document.querySelector('.error-message')
    error.innerHTML = message
    error.style.display = 'block'
}

module.exports = {showError}