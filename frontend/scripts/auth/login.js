'use strict'

import '../../styles/auth/auth.less'
import {showError} from '../help/errorShower'

import axios from 'axios'
import register from './register'

function login(e) {
    let email = document.querySelector('.email').value    
    let password = document.querySelector('.password').value
    if (!(email && password)) {
        showError('Все поля должны быть заполнены')
        return
    }
    let captcha = grecaptcha.getResponse()
    if (!captcha) {
        showError('Заполните капчу')
        return
    }
    let data = createLoginData(email, password)
    setButtonState(true)
    axios.post('/app/Token', queryParams(data))
        .then(response => {
            let date = new Date()
            localStorage.setItem('token', response.data.access_token)
            localStorage.setItem('expires', date.setSeconds(date.getSeconds() + response.data.expires_in))
            localStorage.setItem('email', response.data.email)
            window.location.pathname = '/cabinet';
        })
        .catch(error => {
            showError('Неверный логин или пароль')
            setButtonState(false)
        })    
}

const queryParams = (source) => {
    var array = []  
    for(var key in source) {
       array.push(`${encodeURIComponent(key)}=${encodeURIComponent(source[key])}`)
    }  
    return array.join("&")
}

const createLoginData = function (email, password) {
    return {
        grant_type: 'password',
        username: email,
        password: password
    }
}

const setButtonState = (loading) => {
    let button = document.querySelector('.login-btn')
    let value = loading ? '<i class="fa fa-spinner fa-spin fa-fw"></i>' : 'Войти'
    let padding = loading ? '1vh 3vw' : '1vh 1.5vw'
    button.innerHTML = value
    button.style.padding = padding
}

module.exports = {login, register}