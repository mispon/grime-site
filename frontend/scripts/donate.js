'use strict'

import '../styles/donate.less'

import axios from 'axios'
import md5 from 'md5'
import {isEmailValid} from './help/validator'
import {showError} from './help/errorShower'

const secret = 'cshefupn'
const merchantId = '72625'

function donate(e) {
    let email = document.querySelector('.email').value 
    let amount = document.querySelector('.payment').value   
    if (!inputValid(email, amount)) {
        return
    }
    let captcha = grecaptcha.getResponse()
    if (!captcha) {
        showError('Заполните капчу')
        return
    }
    setButtonState(true)
    axios.get(`/app/api/account/checkAccount?email=${email}`)
        .then(response => {
            if (response.data.success) {
                let orderId = response.data.orderId
                let hash = md5(`${merchantId}:${amount}:${secret}:${orderId}`)
                let url = `http://www.free-kassa.ru/merchant/cash.php?m=${merchantId}&oa=${amount}&o=${orderId}&em=${email}&s=${hash}&lang=ru`
                window.open(url, '_self')
            }
            else {
                showError('Аккаунта с указанной почтой не существует')
                setButtonState(false)
            }
        })
        .catch(error => {
            showError('Произошла ошибка на сервере. Обратитесь к администрации')
            setButtonState(false)
        })
}

const inputValid = (email, amount) => {
    if (!(email && amount)) {
        showError('Все поля должны быть заполнены')
        return false
    }
    if (!isEmailValid(email)) {
        showError('Указана некорректная почта')
        return false
    }
    if (parseInt(amount) <= 0) {
        showError('Сумма должна быть положительной')
        return false
    }
    return true
}

const setButtonState = (loading) => {
    let button = document.querySelector('.donate-btn')
    let value = loading ? '<i class="fa fa-spinner fa-spin fa-fw"></i>' : 'Пополнить'
    let padding = loading ? '1vh 4.35vw' : '1vh 1.5vw'
    button.innerHTML = value
    button.style.padding = padding
}

module.exports = {donate}