'use strict'

import axios from 'axios'

document.addEventListener("DOMContentLoaded", () => {
    axios.get('https://master.mta-v.net/api/servers/detailed')
        .then(response => fillServerInfo(response.data.list))
        .catch(error => console.log('Не удалось загрузить информацию о сервере'))
})

// заполнить форму информацией о сервере
function fillServerInfo(info) {
    info.forEach(item => {
        if (!item.serverName.match(/Grime/i)) {
            return true;
        }
        document.querySelector('.online').innerHTML = `${item.currentPlayers} / ${item.maxPlayers}`;
        return false;
    })
}