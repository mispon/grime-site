'use strict'

let authMenu = '<a href="/cabinet">ВОЙТИ</a><a href="/auth/registration">РЕГИСТРАЦИЯ</a>'
if (window.location.pathname.includes('cabinet')) {
    if (localStorage.getItem('token') !== null && localStorage.getItem('expires') > Date.now()) {
        authMenu = '<a href="/" onclick="clearSession()">ВЫЙТИ</a>'
    }
    else {
        window.location.pathname = '/auth/login'
    }    
}
document.write(`<header><img class="logo" src="/assets/images/logo.png" /><nav><a href="/news">НОВОСТИ</a><a href="/start">НАЧАТЬ ИГРАТЬ</a><a href="/donate">ДОНАТ</a></nav><div class="auth">${authMenu}</div></header>`)
// document.write(`<header><img class="logo" src="/assets/images/logo.png" /><nav><a href="/about">О ПРОЕКТЕ</a><a href="/news">НОВОСТИ</a><a href="/start">НАЧАТЬ ИГРАТЬ</a><a href="/donate">ДОНАТ</a></nav><div class="auth">${authMenu}</div></header>`)

document.querySelector('.logo').onclick = () => {
    window.location.pathname = '/'
}

function clearSession() {
    localStorage.clear()
}