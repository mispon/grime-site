'use strict'

import '../../../styles/cabinet/userInfo.less'

import React from 'react'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import Preview from './preview'
import Footer from './footer'
import { formatDate, toMoney } from '../utils/formatter'

class UserInfo extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            loading: true
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => this.dispatchState(response.data))   
        }
        else {
            this.propsToState(this.props.userInfo)
        }             
    }
    dispatchState(data) {
        this.setState(data)
        this.props.dispatch({
            type: 'SET_DATA',
            state: {userInfo: data}
        })
        this.setState({loading: false})
    }
    propsToState(props) {        
        this.setState({
            Name: props.get('Name'),
            Level: props.get('Level'),
            Email: props.get('Email'),
            Balance: props.get('Balance'),
            Money: props.get('Money'),
            Experience: props.get('Experience'),
            LastLogin: props.get('LastLogin'),
            TotalInGame: props.get('TotalInGame'),
            PremiumEnd: props.get('PremiumEnd'),
            Phone: props.get('Phone'),
            loading: false
        })
        let clanInfo = props.get('ClanInfo')
        if (clanInfo) {
            this.setState({
                ClanInfo: {
                    ClanId: clanInfo.get('ClanId'),
                    Reputation: clanInfo.get('Reputation'),
                    Rank: clanInfo.get('Rank')
                }
            })
        }
    }
    getNameColor() {
        let nameColor = this.props.userInfo.get('NameColor')
        let rgb = nameColor ? nameColor.split(';') : [0, 0, 0]
        return {color: `rgb(${rgb[0]}, ${rgb[1]}, ${rgb[2]})`}
    }
    getClanName(rank) {
        switch(rank) {
            case 1:
                return 'Семья Лос-Сантоса'
            case 2:
                return 'Картель Дяди Ти'
            case 3:
                return 'Короли Долины'
        }
    }
    getRankName(rank) {
        switch(rank) {
            case 1:
                return 'Громила'
            case 2:
                return 'Наемник'
            case 3:
                return 'Гангстер'
            case 4:
                return 'Авторитет'
            case 5:
                return 'Советник'
        }
    }
    render() {
        if (this.state.loading) {
            return null
        }        
        let premium = formatDate(this.state.PremiumEnd, true)
        let premiumActive = new Date(this.state.PremiumEnd) > new Date()
        return(            
            <div className="app-page">
                <Preview />
                <div className="page user-info">
                    <div className="main">
                        <p>{toMoney(this.state.Money, false)}$</p>
                        <p className="exp">Опыт: {this.state.Experience} ед.</p>                        
                    </div>
                    <div className="common">
                        <p style={this.getNameColor()} className="name">{this.state.Name ? this.state.Name : 'Незнакомец'}</p>
                        <span className="level">уровень {this.state.Level}</span>
                        <p className="email">{this.state.Email}</p>                        
                    </div>
                    <p className="clan-name">Банда: {this.state.ClanInfo ? this.getClanName(this.state.ClanInfo.ClanId) : 'не состоит'}</p>  
                    <p className="clan-rank">Звание: {this.state.ClanInfo ? this.getRankName(this.state.ClanInfo.Rank) : 'нет'}</p>  
                    <p className="clan-rep">Репутация: {this.state.ClanInfo ? this.state.ClanInfo.Reputation : 0}</p>  
                    <p className="phone">Телефон: {this.state.Phone > 0 ? this.state.Phone : 'отсутствует'}</p>
                    <p className="premium">Премиум {premiumActive ? `активен до ${premium}` : 'аккаунт не активен'}</p>
                    <p className="time-in-game">Проведено в игре: {this.state.TotalInGame} часов</p>                   
                </div>
                <Footer lastLogin={this.state.LastLogin} balance={this.state.Balance} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

module.exports = connect(mapStateToProps)(UserInfo)