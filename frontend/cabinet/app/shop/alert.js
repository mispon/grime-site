'use sctirct'

import '../../../styles/cabinet/alert.less'

import React from 'react'

class Alert extends React.Component {
    constructor(props) {
        super(props)
    }
    render() {
        let className = this.props.success ? 'alert-success' : 'alert-error'
        return(
            <div className={`alert ${className}`}>
                <p>{this.props.message.toUpperCase()}</p>
            </div> 
        )
    }
}

module.exports = Alert