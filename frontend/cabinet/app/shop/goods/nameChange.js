'use sctirct'

import '../../../../styles/cabinet/goods/nameChange.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import Preview from '../../main/preview'
import Alert from '../alert'
import Footer from '../../main/footer'
import {toMoney} from '../../utils/formatter'
import {isOnlyLatinChars} from '../../../../scripts/help/validator'

class NameChange extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            price: 200,
            valid: false,
            name: '',
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }
    }
    validateName(text) {
        let valid = true
        let values = text.split(' ')
        if (values.length === 2) {            
            values.forEach(value => {
                if (/\d/.test(value) || value.length === 0 || !isOnlyLatinChars(value)) {
                    valid = false
                }
            })
        }
        else {
            valid = false  
        }        
        this.setState({
            valid: valid,
            name: text
        })
    }
    changeName() {
        if (!this.props.userInfo.get('Name')) {
            this.setAlertState({Success: false, Message: 'Вы еще не создали персонажа'})
            return
        }
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }        
        const accountId = this.props.userInfo.get('AccountId')
        axios.post(`/app/api/shop/changeName?accountId=${accountId}&cost=${this.state.price}&name=${this.state.name}`)
            .then(response => {                
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price, name: this.state.name})
                }
                this.setState({name: ''})
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        let name = this.props.userInfo.get('Name')
        if (!name) {
            name = 'Отсутствует'
        }
        return(
            <div className="app-page">
                <Preview />
                <div className='page name-change-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">СМЕНА НИКА</p>
                    <p className="price">{this.state.price} рублей</p>
                    <p className="current-name">Текущий ник:</p>
                    <p className="name">{name}</p>
                    <input 
                        className='new-name' 
                        type='text'
                        maxLength='20'
                        placeholder='Tom Smith' 
                        value= {this.state.name} 
                        onInput={(e) => this.validateName(e.target.value)}
                    />
                    <img className="arrow" src="/assets/images/arrow.png" />
                    <input 
                        className={this.state.valid ? 'buy-button' : 'buy-button disabled'} 
                        type='button'                        
                        onClick={() => this.changeName()} 
                        value='Купить'
                    />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(NameChange)