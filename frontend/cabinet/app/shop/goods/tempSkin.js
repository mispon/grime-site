'use sctirct'

import '../../../../styles/cabinet/goods/tempSkin.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import Preview from '../../main/preview'
import Alert from '../alert'
import Footer from '../../main/footer'
import {toMoney} from '../../utils/formatter'

const PRICE_FOR_ONE = 30

class TempSkin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            count: 1,
            valid: true,
            price: PRICE_FOR_ONE,
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }
    }
    onCountChange(count) {
        if (count) {
            count = count >= 1 ? count : 1
            this.setState({
                count: count,
                valid: true,
                price: count * PRICE_FOR_ONE
            })
        }
        else {
            this.setState({count: '', valid: false})
        }
        
    }
    buySkins() {
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }
        const accountId = this.props.userInfo.get('AccountId')
        axios.post(`/app/api/shop/buyTempSkin?accountId=${accountId}&cost=${this.state.price}&count=${this.state.count}`)
            .then(response => {
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price})
                }
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        return(
            <div className="app-page">
                <Preview />
                <div className='page temp-skin-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">ВРЕМЕННЫЙ СКИН</p>
                    <p className="price">{this.state.price} рублей</p>
                    <div className="description">
                        <p>Покупаемый предмет добавляется в ваш инвентарь.</p>
                        <p>После активации, вы случайным образом перевоплощаетесь в один из многочисленных скинов на <b>1 час</b>.</p>
                        <p>При выходе из игры, эффект от перевоплощения сбрасывается.</p>
                    </div>
                    <div className="count">
                        <span>КОЛИЧЕСТВО</span>
                        <input 
                            type="number"
                            value={this.state.count}
                            onChange={(e) => this.onCountChange(e.target.value)}                            
                            min="1"
                        />
                    </div>
                    <input 
                        className={this.state.valid ? 'buy-button' : 'buy-button disabled'}
                        type='button' 
                        onClick={() => this.buySkins()} 
                        value='Купить'
                        readOnly
                    />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(TempSkin)