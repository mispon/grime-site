'use sctirct'

import '../../../../styles/cabinet/goods/phoneChange.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import Preview from '../../main/preview'
import Alert from '../alert'
import Footer from '../../main/footer'
import {toMoney} from '../../utils/formatter'

const regex = /^(?=.{6,6}$)[\d]*$/
const MIN_NUMBER = 100000

class PhoneNumber extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            price: 100,
            valid: false,
            number: '',
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }
    }
    validateNumber(number) {
        this.setState({
            valid: regex.test(number) && number >= MIN_NUMBER,
            number: number
        })
    }
    changeNumber() {
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }
        const accountId = this.props.userInfo.get('AccountId')
        axios.post(`/app/api/shop/changePhone?accountId=${accountId}&cost=${this.state.price}&number=${this.state.number}`)
            .then(response => {
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price, number: this.state.number})
                }
                this.setState({number: ''})
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        let phone = this.props.userInfo.get('Phone')
        return(
            <div className="app-page">             
                <Preview />
                <div className='page phone-change-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">НОМЕР ТЕЛЕФОНА</p>
                    <p className="price">{this.state.price} рублей</p>
                    <p className="current-phone">Текущий номер:</p>
                    <p className="phone">{phone > 0 ? phone : 'НЕТ'}</p>
                    <input 
                        className='new-phone'
                        type='number'
                        placeholder='800000'
                        value={this.state.number}
                        onInput={(e) => this.validateNumber(e.target.value)} 
                    />
                    <img className="arrow" src="/assets/images/arrow.png" />
                    <input 
                        className={this.state.valid ? 'buy-button' : 'buy-button disabled'} 
                        type='button' 
                        onClick={() => this.changeNumber()} 
                        value='Купить'
                    />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(PhoneNumber)