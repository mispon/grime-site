'use sctirct'

import '../../../../styles/cabinet/goods/premium.less'

import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import axios from 'axios'
import { isUndefined } from 'util'

import actions from '../../store/actions'
import Preview from '../../main/preview'
import Alert from '../alert'
import Footer from '../../main/footer'
import {toMoney} from '../../utils/formatter'

class Premium extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            days: 1,
            price: 50,
            showAlert: false,
            alertMessage: '',
            actionSuccess: false
        }
    }
    componentWillMount() {
        if (isUndefined(this.props.userInfo)) {
            const email = localStorage.getItem('email')
            axios.get(`/app/api/account/getInfo?email=${email}`).then(response => {
                this.props.dispatch({
                    type: 'SET_DATA',
                    state: {userInfo: response.data}
                })
            })   
        }
    }
    onDaysSelect(days) {
        this.setState({
            days: days, 
            price: this.getPrice(days)
        })
    }
    buyPremium() {
        if (this.props.userInfo.get('Balance') < this.state.price) {
            this.setAlertState({Success: false, Message: 'На балансе недостаточно средств'})
            return
        }
        const accountId = this.props.userInfo.get('AccountId')
        axios.post(`/app/api/shop/buyPremium?accountId=${accountId}&cost=${this.state.price}&days=${this.state.days}`)
            .then(response => {
                this.setAlertState(response.data)
                if (response.data.Success) {
                    this.props.updateState({price: this.state.price})
                }
            })
            .catch(error => this.setAlertState({Success: false, Message: 'Неполадки на сервере'}))
    }
    getPrice(days) {
        switch(days) {
            case 1:
                return 50
            case 7:
                return 300
            case 30:
                return 900
        }
    }
    setAlertState(data) {
        this.setState({
            showAlert: true,
            actionSuccess: data.Success,
            alertMessage: data.Message
        })
    }
    render() {
        if (isUndefined(this.props.userInfo)) {
            return null
        }
        return(
            <div className="app-page">
                <Preview />
                <div className='page premium-page'>
                    {this.state.showAlert && <Alert success={this.state.actionSuccess} message={this.state.alertMessage} />}
                    <p className="good-name">ПРЕМИУМ АККАУНТ</p>
                    <p className="price">{this.state.price} рублей</p>
                    <div className="premium-types">
                        <input type="radio" name="prem" id="prem-day" onClick={() => this.onDaysSelect(1)} defaultChecked />
                        <label htmlFor="prem-day">ДЕНЬ</label>
                        <input type="radio" name="prem" id="prem-week" onClick={() => this.onDaysSelect(7)} />
                        <label htmlFor="prem-week">НЕДЕЛЯ</label>
                        <input type="radio" name="prem" id="prem-month" onClick={() => this.onDaysSelect(30)} />
                        <label htmlFor="prem-month">МЕСЯЦ</label>
                    </div>
                    <ul className="description">                        
                        <li>50% к зарплате на любой работе</li>
                        <li>20% к опыту любой работы</li>
                        <li>25% к выплатам от банды</li>
                        <li>50% к опыту персонажа</li>
                        <li>отсутствие штрафа за смерть</li>
                        <li>30% к получаемому рейтингу в банде</li>
                    </ul>               
                    <input className="buy-button" type="button" onClick={() => this.buyPremium()} value="Купить" />
                </div>
                <Footer balance={this.props.userInfo.get('Balance')} />
            </div>
        )
    }
}

function mapStateToProps(state) {
    return {
        userInfo: state.get('userInfo')
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        updateState: actions.updateState,
        dispatch: dispatch
    }, dispatch)
}

module.exports = connect(mapStateToProps, mapDispatchToProps)(Premium)