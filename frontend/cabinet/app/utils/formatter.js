'use strict'

/*
** Форматирование даты
*/
const formatDate = (dateValue, withHours) => {
    let t = dateValue.split(/[- : T]/)
    let parseDate = new Date(
        parseInt(t[0]), parseInt(t[1] - 1), parseInt(t[2]), parseInt(t[3]), parseInt(t[4]), parseInt(t[5])
    )
    let date = new Date(parseDate)
    let dd = getTwoDigits(date.getDate())
    let mm = getTwoDigits(date.getMonth() + 1)
    let yyyy = date.getFullYear()
    let result = `${dd}.${mm}.${yyyy}`
    if (withHours) {
        let hh = getTwoDigits(date.getHours())
        let mins = getTwoDigits(date.getMinutes())
        result = `${result} ${hh}:${mins}`
    }
    return result
}

const getTwoDigits = (num) => {
    if (num < 10) {
        num = '0' + num
    }
    return num
}

/*
** Форматирование денег
*/
const toMoney = (money, needFraction) => {
    let digitsCount = needFraction ? 2 : 0
    return parseFloat(money).toLocaleString('ru-RU', {maximumFractionDigits: digitsCount})
}

module.exports = {formatDate, toMoney}