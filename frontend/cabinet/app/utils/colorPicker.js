'use strict'

import '../../../styles/cabinet/colorPicker.less'

import React from 'react'

class ColorPicker extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        red: 0,
        green: 0,
        blue: 0
      };
    }
    update(state) {
      this.setState(state);
      this.props.colorHandler(state)
    }
    render() {
        const background = `rgb(${this.state.red}, ${this.state.green}, ${this.state.blue})`
        let myStyle = {
            backgroundColor: background
        };       
        return (
            <div className="color-picker">
                <Slider ref="red" min="0" max="255" col="R" val={this.state.red} update={(e) => this.update({red: e.target.value})}>
                    {this.state.red}
                </Slider>         
                <Slider ref="green" min="0" max="255" col="G" val={this.state.green} update={(e) => this.update({green: e.target.value})}>
                    {this.state.green}
                </Slider> 
                <Slider ref="blue" min="0" max="255" col="B" val={this.state.blue} update={(e) => this.update({blue: e.target.value})}>
                    {this.state.blue}
                </Slider>
            </div>
        );
    }
  }
  
  class Slider extends React.Component {
    render() {
      return (
        <div>
            <div className="color-info">
                <span>{this.props.col}</span>
                <span className="color-value">{this.props.children}</span>
            </div>
            <input ref="input" 
              value={this.props.val} 
              type="range" 
              min={this.props.min} 
              max={this.props.max} 
              step={this.props.step} 
              onChange={this.props.update} 
            />
       </div>
      )
    }
  }  

module.exports = ColorPicker