'use sctirct'

import '../../styles/cabinet/cabinet.less'

import React from 'react'
import ReactDOM from 'react-dom'
import { Switch, Route } from 'react-router';
import { HashRouter, Link } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import reducer from './store/reducer'

import UserInfo from './main/userInfo'
import Shop from './shop/shop'
import NameChange from './shop/goods/nameChange'
import NameColor from './shop/goods/nameColor'
import PhoneNumber from './shop/goods/phoneNumber'
import Premium from './shop/goods/premium'
import SkinReset from './shop/goods/skinReset'
import TempSkin from './shop/goods/tempSkin'

let store = createStore(reducer)

const App = () => (
    <div className="cabinet-app">
        <ul className="cabinet-nav">
            <li className="user-info-link"><Link to='/user'>АККАУНТ</Link></li>
            <li className="shop-link"><Link to='/shop'>МАГАЗИН</Link></li>                      
        </ul>
        <Switch>
            <Route path='/shop/name-change' component={NameChange} />
            <Route path='/shop/name-color' component={NameColor} />
            <Route path='/shop/phone-number' component={PhoneNumber} />
            <Route path='/shop/premium' component={Premium} />
            <Route path='/shop/skin-reset' component={SkinReset} />
            <Route path='/shop/temp-skin' component={TempSkin} />

            <Route path='/user' component={UserInfo} />
            <Route path='/shop' component={Shop} />
            <Route exact path='/' component={UserInfo} />                     
        </Switch>        
    </div>
)

ReactDOM.render((
    <HashRouter>
        <Provider store={store}>
            <App />
        </Provider>
    </HashRouter>
), document.getElementById('app'))