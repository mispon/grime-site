'use strict'

const updateState = (state) => {
    return {
        type: 'UPDATE_STATE',
        price: state.price,
        name: state.name,
        number: state.number,
        color: state.color
    }
}

module.exports = {updateState}