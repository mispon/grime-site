﻿using backend.Entities;
using gta_mp_data.Entity;
using LinqToDB;
using LinqToDB.Data;

namespace backend {
    /// <summary>
    /// Подключения к базе данных
    /// </summary>
    public class Database : DataConnection {
        public Database() : base("GtaVServer") {}

        public ITable<Payment> Payments => GetTable<Payment>();
        public ITable<Account> Accounts => GetTable<Account>();
        public ITable<PlayerInfo> PlayerInfos => GetTable<PlayerInfo>();
        public ITable<PlayerAdditionalInfo> PlayerAdditionalInfos => GetTable<PlayerAdditionalInfo>();
        public ITable<PlayerWork> PlayerWorks => GetTable<PlayerWork>();
        public ITable<InventoryItem> Inventory => GetTable<InventoryItem>();
        public ITable<PlayerClanInfo> ClanInfos => GetTable<PlayerClanInfo>();
    }
}