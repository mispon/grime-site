﻿using System.Web;
using System.Web.Http;
using backend.Managers.Interfaces;

namespace backend.Controllers {
    /// <summary>
    /// Контроллер оплаты
    /// </summary>
    public class PaymentsController : ApiController {
        private readonly IPaymentsManager _paymentsManager;

        public PaymentsController(IPaymentsManager paymentsManager) {
            _paymentsManager = paymentsManager;
        }

        /// <summary>
        /// Обработка платежей
        /// </summary>
        [HttpGet]
        public IHttpActionResult Get() {
            var nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            var result = _paymentsManager.CreatePayment(nvc);
            object response;
            if (result) {
                response = new {result = new {message = "Платеж успешно завершен"}};
            }
            else {
                response = new { error = new { message = "Не удалось завершить платеж" } };
            }
            return Json(response);
        }
    }
}