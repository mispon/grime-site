﻿using System.Web.Http;
using backend.Managers.Interfaces;

namespace backend.Controllers {
    public class ShopController : ApiController {
        private readonly IShopManager _shopManager;

        public ShopController(IShopManager shopManager) {
            _shopManager = shopManager;
        }

        /// <summary>
        /// Обработчик покупки премиума
        /// </summary>
        [HttpPost]
        public IHttpActionResult BuyPremium(long accountId, int cost, int days) {
            var result = _shopManager.BuyPremium(accountId, cost, days);
            return Json(result);
        }

        /// <summary>
        /// Обработчик смены ника
        /// </summary>
        [HttpPost]
        public IHttpActionResult ChangeName(long accountId, int cost, string name) {
            var result = _shopManager.ChangeName(accountId, cost, name);
            return Json(result);
        }

        /// <summary>
        /// Обработчик покупки цвета ника
        /// </summary>
        [HttpPost]
        public IHttpActionResult ChangeColor(long accountId, int cost, string color) {
            var result = _shopManager.ChangeNameColor(accountId, cost, color);
            return Json(result);
        }

        /// <summary>
        /// Обработчик смены номера телефона
        /// </summary>
        [HttpPost]
        public IHttpActionResult ChangePhone(long accountId, int cost, int number) {
            var result = _shopManager.ChangePhoneNumber(accountId, cost, number);
            return Json(result);
        }

        /// <summary>
        /// Обработчик сброса внешности
        /// </summary>
        [HttpPost]
        public IHttpActionResult ResetSkin(long accountId, int cost) {
            var result = _shopManager.ResetSkin(accountId, cost);
            return Json(result);
        }

        /// <summary>
        /// Обработчик покупки временных скинов
        /// </summary>
        [HttpPost]
        public IHttpActionResult BuyTempSkin(long accountId, int cost, int count) {
            var result = _shopManager.AddTemporarySkin(accountId, cost, count);
            return Json(result);
        }
    }
}