﻿using System.IO;
using System.Net;
using System.Web.Http;

namespace backend.Controllers {
    public class NewsController : ApiController {
        private const string URL = 
            "https://api.vk.com/method/wall.get?owner_id=-140947332&count=50&access_token=43953b9843953b9843953b98d743c824534439543953b981af2dfba9db6b8153c74c096&v=5.67";

        /// <summary>
        ///     Получить список новостей
        /// </summary>
        [HttpPost]
        public IHttpActionResult GetNews() {
            var request = WebRequest.Create(URL);
            var response = request.GetResponse();
            var stream = response.GetResponseStream();
            string content;
            using (var reader = new StreamReader(stream ?? Stream.Null)) {
                content = reader.ReadToEnd();
            }
            return Json(new {content});
        }
    }
}