﻿using System;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using LinqToDB.Mapping;

namespace backend.Entities {
    /// <summary>
    /// Платеж
    /// </summary>
    [Table(Name = "Payments")]
    public class Payment {
        public Payment() {}
        public Payment(NameValueCollection nvc) {
            InitId = nvc["intid"];
            Email = nvc["P_EMAIL"];
            Phone = nvc["P_PHONE"];
            Amount = (int) double.Parse(nvc["AMOUNT"], CultureInfo.InvariantCulture);
            if (nvc.AllKeys.Contains("CUR_ID")) {
                Сurrency = (int) double.Parse(nvc["CUR_ID"], CultureInfo.InvariantCulture);
            }
            Date = DateTime.Now;
        }

        /// <summary>
        /// Уникальный идентификатор платежа
        /// </summary>
        [Column(Name = "Id"), PrimaryKey, Identity]
        public int Id { get; set; }

        /// <summary>
        /// Идентификатор абонента в системе Free-Kassa
        /// </summary>
        [Column(Name = "InitId")]
        public string InitId { get; set; }

        /// <summary>
        /// Почта плательщика
        /// </summary>
        [Column(Name = "Email")]
        public string Email { get; set; }

        /// <summary>
        /// Телефон плательщика (передается только для мобильных платежей)
        /// </summary>
        [Column(Name = "Phone")]
        public string Phone { get; set; }

        /// <summary>
        /// Сумма заказа
        /// </summary>
        [Column(Name = "Amount")]
        public int Amount { get; set; }

        /// <summary>
        /// Валюта, которой оплачивали
        /// </summary>
        [Column(Name = "Сurrency")]
        public int Сurrency { get; set; }

        /// <summary>
        /// Дата создания платежа
        /// </summary>
        [Column(Name = "Date")]
        public DateTime Date { get; set; }
    }
}