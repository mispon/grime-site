﻿using System.Threading.Tasks;
using gta_mp_data.Entity;
using PlayerInfo = backend.Models.PlayerInfo;

namespace backend.Providers.Interfaces {
    public interface IAccountsProvider {
        /// <summary>
        /// Получить аккаунт по почте и паролю
        /// </summary>
        Task<Account> GetAccountAsync(string email, string password);

        /// <summary>
        /// Получить данные аккаунта
        /// </summary>
        Account Get(long accountId);

        /// <summary>
        /// Записать данные аккаунта
        /// </summary>
        bool Set(Account account);

        /// <summary>
        /// Возвращает суммарную информацию об аккаунте
        /// </summary>
        PlayerInfo GetInfo(string email);

        /// <summary>
        /// Проверяет существование аккаунта
        /// </summary>
        bool IsAccountExist(string email);

        /// <summary>
        /// Обновить баланс аккаунта
        /// </summary>
        bool UpdateBalance(long accountId, int balance);

        /// <summary>
        /// Обновить баланс аккаунта
        /// </summary>
        bool UpdateBalance(string email, int balance);

        /// <summary>
        /// Проверяет, достаточно ли денег на балансе
        /// </summary>
        bool EnoughBalance(long accountId, int cost);

        /// <summary>
        /// Добавляет новый аккаунт
        /// </summary>
        bool AddAccount(string email, string password, string friendReferal);
    }
}