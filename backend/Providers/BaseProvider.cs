﻿using System;

namespace backend.Providers {
    /// <summary>
    /// Общая логика всех провайдеров
    /// </summary>
    public class BaseProvider {
        /// <summary>
        /// Безопасное выполнение запроса
        /// </summary>
        protected static bool SafetyExecute(Action action) {
            try {
                action();
                return true;
            }
            catch (Exception) {
                return false;
            }
        }
    }
}