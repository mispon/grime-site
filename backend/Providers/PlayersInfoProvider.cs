﻿using System.Linq;
using backend.Providers.Interfaces;
using gta_mp_data.Entity;
using gta_mp_data.Enums;
using LinqToDB;
// ReSharper disable AccessToDisposedClosure

namespace backend.Providers {
    /// <summary>
    /// Логика
    /// </summary>
    public class PlayersInfoProvider : BaseProvider, IPlayersInfoProvider {
        /// <summary>
        /// Возвращает основные данные игрока
        /// </summary>
        public PlayerInfo Get(long accountId) {
            using (var db = new Database()) {
                return db.PlayerInfos.First(e => e.AccountId == accountId);
            }
        }

        /// <summary>
        /// Записывает данные игрока
        /// </summary>
        public bool Set(PlayerInfo playerInfo) {
            using (var db = new Database()) {
                return SafetyExecute(() => db.Update(playerInfo));
            }
        }

        /// <summary>
        /// Возвращает дополнительные данные игрока
        /// </summary>
        public PlayerAdditionalInfo GetAdditional(long accountId) {
            using (var db = new Database()) {
                return db.PlayerAdditionalInfos.First(e => e.AccountId == accountId);
            }
        }

        /// <summary>
        /// Записывает дополнительные данные игрока
        /// </summary>
        public bool SetAdditional(PlayerAdditionalInfo playerAdditionalInfo) {
            using (var db = new Database()) {
                return SafetyExecute(() => db.Update(playerAdditionalInfo));
            }
        }

        /// <summary>
        /// Сбрасывает имя персонажа
        /// </summary>
        public bool ResetName(long accountId) {
            using (var db = new Database()) {
                return SafetyExecute(
                    () => db.PlayerInfos
                            .Where(e => e.AccountId == accountId)
                            .Set(e => e.Name, string.Empty)
                            .Update()
                );
            }
        }

        /// <summary>
        /// Добавляет временный скин в инвентарь
        /// </summary>
        public bool AddTempSkins(long accountId, int count) {
            using (var db = new Database()) {
                var item = db.Inventory.FirstOrDefault(e => e.OwnerId == accountId && e.Type == InventoryType.TempoSkin);
                return SafetyExecute(() => {
                    if (item == null) {
                        var skinItem = new InventoryItem {
                            OwnerId = accountId,
                            Name = "Временный скин",
                            Type = InventoryType.TempoSkin,
                            Count = count
                        };
                        db.Insert(skinItem);
                    }
                    else {
                        item.Count += count;
                        db.Update(item);
                    }
                });
            }
        }

        /// <summary>
        /// Проверяет, что имя доступно
        /// </summary>
        public bool NameAvailable(string name) {
            using (var db = new Database()) {
                return db.PlayerInfos.All(e => e.Name != name);
            }
        }

        /// <summary>
        /// Проверяет, что номер доступен
        /// </summary>
        public bool NumberAvailable(int number) {
            using (var db = new Database()) {
                return db.PlayerAdditionalInfos.All(e => e.PhoneNumber != number);
            }
        }
    }
}