﻿using backend.Models;

namespace backend.Managers.Interfaces {
    public interface IShopManager {
        /// <summary>
        /// Покупка премиума
        /// </summary>
        OperationResult BuyPremium(long accountId, int cost, int days);

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        OperationResult ChangeName(long accountId, int cost, string newName);

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        OperationResult ChangeNameColor(long accountId, int cost, string color);

        /// <summary>
        /// Смена имени персонажа
        /// </summary>
        OperationResult ChangePhoneNumber(long accountId, int cost, int phoneNumber);

        /// <summary>
        /// Сброс внешности
        /// </summary>
        OperationResult ResetSkin(long accountId, int cost);

        /// <summary>
        /// Добавляет временные скины в инвентарь
        /// </summary>
        OperationResult AddTemporarySkin(long accountId, int cost, int count);
    }
}