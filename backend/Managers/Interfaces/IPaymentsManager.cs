﻿using System.Collections.Specialized;

namespace backend.Managers.Interfaces {
    public interface IPaymentsManager {
        /// <summary>
        /// Подтверждает оплату платежа
        /// </summary>
        bool CreatePayment(NameValueCollection nvc);

        /// <summary>
        /// Возвращает идентификатор последнего платежа
        /// </summary>
        /// <returns></returns>
        int GetLastPaymentId();
    }
}