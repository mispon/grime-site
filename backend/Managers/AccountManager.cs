﻿using backend.Managers.Interfaces;
using backend.Models;
using backend.Providers.Interfaces;

namespace backend.Managers {
    /// <summary>
    /// Логика работы с аккаунтом
    /// </summary>
    public class AccountManager : IAccountManager {
        private readonly IAccountsProvider _accountsProvider;

        public AccountManager(IAccountsProvider accountsProvider) {
            _accountsProvider = accountsProvider;
        }

        /// <summary>
        /// Возвращает суммарную информацию об аккаунте
        /// </summary>
        public PlayerInfo GetInfo(string email) {
            return _accountsProvider.GetInfo(email);
        }

        /// <summary>
        /// Обновить баланс аккаунта
        /// </summary>
        public bool UpdateBalance(string email, int balance) {
            return _accountsProvider.UpdateBalance(email, balance);
        }

        /// <summary>
        /// Проверяет наличие аккаунта
        /// </summary>
        public bool IsAccountExist(string email) {
            return _accountsProvider.IsAccountExist(email);
        }

        /// <summary>
        /// Создает новый аккаунт
        /// </summary>
        public OperationResult Register(string email, string password, string referal) {
            if (IsAccountExist(email)) {
                return new OperationResult {Success = false, Message = "Аккаунт с такой почтой уже существует"};
            }
            var result = _accountsProvider.AddAccount(email, password, referal);
            var message = result ? "Аккаунт успешно создан" : "Возникла проблема на сервере. Обратитесь к администрации";
            return new OperationResult {Success = result, Message = message};
        }
    }
}