﻿using System.Collections.Specialized;
using System.Security.Cryptography;
using System.Text;
using backend.Entities;
using backend.Managers.Interfaces;
using backend.Providers.Interfaces;
// ReSharper disable All

namespace backend.Managers {
    /// <summary>
    /// Логика пополнения баланса
    /// </summary>
    public class PaymentsManager : IPaymentsManager {
        private const string SECRET = "2txa9c7w";

        private readonly IPaymentsProvider _paymentsProvider;
        private readonly IAccountManager _accountManager;

        public PaymentsManager(IPaymentsProvider paymentsProvider, IAccountManager accountManager) {
            _paymentsProvider = paymentsProvider;
            _accountManager = accountManager;
        }

        /// <summary>
        /// Создает платеж
        /// </summary>
        public bool CreatePayment(NameValueCollection nvc) {
            var payment = new Payment(nvc);
            if (GetHash(nvc) != nvc["SIGN"]) {
                return false;
            }
            if (_paymentsProvider.IsExist(payment.InitId)) {
                return false;
            }
            var confirmResult = _paymentsProvider.Add(payment);
            if (!confirmResult) {
                return false;
            }
            return _accountManager.UpdateBalance(payment.Email, payment.Amount);
        }

        /// <summary>
        /// Возвращает идентификатор последнего платежа
        /// </summary>
        /// <returns></returns>
        public int GetLastPaymentId() {
            return _paymentsProvider.GetLastPaymentId();
        }

        /// <summary>
        /// Возвращает md5 хэш
        /// </summary>
        private string GetHash(NameValueCollection nvc) {
            var merchantId = nvc["MERCHANT_ID"];
            var amount = nvc["AMOUNT"];
            var orderId = nvc["MERCHANT_ORDER_ID"];
            return CreateMD5($"{merchantId}:{amount}:{SECRET}:{orderId}").ToString().ToLower();
        }

        /// <summary>
        /// Создает md5 хэш из переданной строки
        /// </summary>
        private static string CreateMD5(string input) {
            using (var md5 = MD5.Create()) {
                var inputBytes = Encoding.ASCII.GetBytes(input);
                var hashBytes = md5.ComputeHash(inputBytes);
                var sb = new StringBuilder();
                foreach (var bt in hashBytes) {
                    sb.Append(bt.ToString("X2"));
                }
                return sb.ToString();
            }
        }
    }
}