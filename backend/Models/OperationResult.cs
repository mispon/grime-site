﻿namespace backend.Models {
    /// <summary>
    /// Результат выполнения запроса
    /// </summary>
    public class OperationResult {
        /// <summary>
        /// Признак выполнения
        /// </summary>
        public bool Success { get; set; }

        /// <summary>
        /// Сообщение
        /// </summary>
        public string Message { get; set; }
    }
}